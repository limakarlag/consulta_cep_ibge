Dado ("o que número do CEP é existente e válido {string}") do |cep| 
    @cep = cep
end

Quando ("realizar uma consulta para buscar o CEP de uma localidade") do
    @response = HTTParty.get("https://viacep.com.br/ws/#{@cep}/json/")
end

Então ("a API retorna o código do IBGE") do 
    puts JSON.parse(@response.body)['ibge']
end

Dado ("o que número do CEP é inexistente e válido {string}")  do |cep| 
    @cep = cep    
end

Então ("a API retorna erro: true com status 200") do
    expect(@response.code).to eq(200)
    expect(JSON.parse(@response.body)['erro']).to eq(true)
end

Dado ("o que número do CEP é inválido {string}") do |cep|
    @cep = cep
end

Então ("a API não retorna o código do IBGE") do
    expect(@response.code).to eq(400) 
end
