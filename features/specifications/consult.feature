#language: pt

Funcionalidade: Consultar dados de um endereço
Como usuário do sistema
Eu quero realizar requisições na API
Para fim de encontrar código do IBGE do endereço a partir de um CEP

Cenário: Consultar um CEP existente e válido
    Dado o que número do CEP é existente e válido "01001000"
    Quando realizar uma consulta para buscar o CEP de uma localidade
    Então a API retorna o código do IBGE 

Cenário: Consultar um CEP inexistente e válido
    Dado o que número do CEP é inexistente e válido "99999999"
    Quando realizar uma consulta para buscar o CEP de uma localidade
    Então a API retorna erro: true com status 200

Cenário: Consultar um CEP inválido
    Dado o que número do CEP é inválido "123"
    Quando realizar uma consulta para buscar o CEP de uma localidade
    Então a API não retorna o código do IBGE
