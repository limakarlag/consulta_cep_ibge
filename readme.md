# Desafio Consulta CEP

O teste foi desenvolvido com a finalidade de acessar a webservice ViaCep; que tem como finalidade a consulta e exibição de dados relacionados ao código do endereçamento postal; e como resultado das consultas feitas no teste Desafio Consulta CEP, exibir o código do IBGE.

## Instruções

1. Instale o ruby (segue exemplo utilizando o gerenciador de pacotes do Debian);
```batch
sudo apt-get install ruby
```
2. Instale o bundler;
```batch
gem install bundler
```
3. Instale as gems do projeto;
```batch
bundle install
```
4. Rode os testes.
```batch
cucumber
```
